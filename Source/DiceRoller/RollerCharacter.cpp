// Fill out your copyright notice in the Description page of Project Settings.

#include "RollerCharacter.h"

// Sets default values
ARollerCharacter::ARollerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARollerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARollerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARollerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

